# Visual Basic Analyzer - Specs

## Visual Basic 6

* [Visual Basic 6.0 Reference](https://learn.microsoft.com/en-us/previous-versions/visualstudio/visual-basic-6/visual-basic-6.0-documentation) on the Microsoft Visual Basic website.

## VB.NET 11

* [Visual Basic 11.0 Language Specification](https://www.microsoft.com/en-us/download/details.aspx?id=15039) on the Microsoft Download pages.
