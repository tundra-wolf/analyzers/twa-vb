use std::path::PathBuf;

use clap::{Parser, ValueEnum};

// Visual Basic language settings
#[derive(Clone, Debug, Parser)]
pub struct VisualBasicSettings {
    /// VB version
    #[arg(
        long = "release",
        short = 'r',
        default_value_t = VisualBasicVersion::VbNet11,
        value_enum
    )]
    version: VisualBasicVersion,

    /// Source file
    source_file: PathBuf,

    /// Project ID
    project_id: Option<String>,
}

/// Supported Visual Basic versions.
#[derive(Clone, Debug, ValueEnum)]
pub enum VisualBasicVersion {
    /// Visual Basic 6
    Vb6,

    /// Visual Basic.NET 11
    VbNet11,
}

impl Default for VisualBasicVersion {
    fn default() -> Self {
        Self::VbNet11
    }
}
