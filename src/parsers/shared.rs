use tw_data::literals::{
    CharacterLiteral, DateLiteral, DecimalLiteral, FloatingPointLiteral, IntegerLiteral,
    StringLiteral,
};

// Visual Basic literals
#[derive(Debug)]
pub enum Literal {
    Bool(bool),
    Character(CharacterLiteral),
    Date(DateLiteral),
    Decimal(DecimalLiteral),
    Integer(IntegerLiteral),
    FloatingPoint(FloatingPointLiteral),
    String(StringLiteral),
    Nothing,
}
